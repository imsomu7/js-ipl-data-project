// Number matches won per year :
const csvtojson = require("csvtojson");
const matchesPath = ("../data/matches.csv");
const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {

        let wonPerYear = {};
        for (let match of matches) {
            if (wonPerYear[match.season] === undefined) {
                wonPerYear[match.season] = won(match.season);
                
            }
        }
        function won(wonPerYear) {
            let wonmatches = {};
            for (let match of matches) {
                if (wonmatches[match.winner] === undefined && match.season === wonPerYear) {
                    wonmatches[match.winner] = 1
                }
                else {
                    if (wonPerYear === match.season)
                        wonmatches[match.winner] += 1;
                }
            }
            return wonmatches;
        }
        //console.log(wonPerYear);

        fs.writeFileSync("../public/output/matchWonPerteamPerYear.json", JSON.stringify(wonPerYear));
    });
