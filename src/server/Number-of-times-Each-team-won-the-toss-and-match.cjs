const csvtojson = require("csvtojson");
const matchesPath = '../data/matches.csv';
const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        // console.log(matches);
        const tossWonTeam = matches.reduce((acc, curr) => {
            if (curr.toss_winner === curr.winner) {
                if (acc[curr.toss_winner] === undefined && acc[curr.winner] === undefined) {
                    acc[curr.winner] = 1;
                }
                else {
                    acc[curr.winner] += 1;
                }
            }
            return acc;
        }, {});
        //console.log(tossWonTeam);
        fs.writeFileSync("../public/output/numberOfTimesEachTeamWonTossAndMatch.json", JSON.stringify(tossWonTeam));

    });