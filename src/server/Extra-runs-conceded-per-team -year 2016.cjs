const csvtojson = require("csvtojson");
const deliveryPath = ("../data/deliveries.csv");
const matchesPath = ("../data/matches.csv");

const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        csvtojson()
            .fromFile(deliveryPath)
            .then((deliveries) => {
                // console.log(deliveries);
                const result = extraRunsEachTeams(matches, deliveries);
                //console.log(result);
                fs.writeFileSync('../public/output/extraRunsConcededPerTeam.json', JSON.stringify(result));
            })
    })
function extraRunsEachTeams(matches, deliveries) {
    let id2016 = matches.filter((matches) => {
        if (matches.season === "2016") {
            return matches;
        }
    });
    //console.log(id2016);

    let deliveriesId = [];
    id2016.forEach((match) => {
        console.log(match);
        deliveries.filter((delivery) => {
            if (match.id === delivery.match_id) {
                deliveriesId.push(delivery);
            }
        })
    })
    //console.log(deliveriesId);
    const extraRuns = deliveriesId.reduce((acc,curr)=>{
        if(acc[curr.bowling_team] === undefined){
            acc[curr.bowling_team] = parseInt(curr.extra_runs)
        }else{
            acc[curr.bowling_team] +=parseInt(curr.extra_runs);
        }
        return acc
    },{})
    return extraRuns;
}
