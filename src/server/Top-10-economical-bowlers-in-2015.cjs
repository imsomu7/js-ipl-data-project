const { match } = require("assert");
const csvtojson = require("csvtojson");
const deliveryPath = ("../data/deliveries.csv");
const matchesPath = ("../data/matches.csv");

const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        csvtojson()
            .fromFile(deliveryPath)
            .then((deliveries) => {
                //console.log(deliveries);
                const result = economicalBowlers(matches, deliveries);
               // console.log(result);
               fs.writeFileSync('../public/output/top10economicbowlers2015.json', JSON.stringify(result));

            });
    });
function economicalBowlers(matches, deliveries) {
    const id2015 = matches.filter((match) => {
        if (match.season === "2015") {
            //console.log(match);
            return match;
        }
    });
    let deliveriesId = [];
    id2015.forEach((match) => {
        // console.log(match);
        deliveries.filter((delivery) => {
            if (match.id === delivery.match_id) {
                deliveriesId.push(delivery);
            }
        })
        //console.log(deliveriesId)
    });
    const balls = deliveriesId.reduce((acc, curr) => {
        if (curr.noball_runs == 0 || curr.wide_runs == 0) {
            if (acc[curr.bowler] === undefined) {
                acc[curr.bowler] = 1;
            }
            else {
                acc[curr.bowler] = acc[curr.bowler] + 1;
            }

        }
        return acc;
    }, {});
    //console.log(balls);
    const totalRuns = deliveriesId.reduce((acc, curr) => {
        if (acc[curr.bowler] === undefined) {
            acc[curr.bowler] = parseInt(curr.total_runs);
        }
        else {
            acc[curr.bowler] = acc[curr.bowler] + parseInt(curr.total_runs);
        }
        return acc;
    });
    //console.log(totalRuns);
    let economy = {};
    for (let ball in balls) {
        if (economy[ball] === undefined) {
            economy[ball] = parseFloat(((totalRuns[ball] * 6) / balls[ball]).toFixed(2));
        }
    }
    //return economy;
    //console.log(economy);

    let bowlerEconomy = Object.keys(economy).map(function (key) {
        return [key, economy[key]];
    }).sort(function (a, b) { return a[1] - b[1]; });
   //console.log(bowlerEconomy);
   const top10EconomyBowlers = {};
    for (let eco=0;eco<10;eco++){
       // console.log(bowlerEconomy[eco]);
       
       if (top10EconomyBowlers[bowlerEconomy[eco][0]] == undefined){
         top10EconomyBowlers[bowlerEconomy[eco][0]] = bowlerEconomy[eco][1];
       // return top10EconomyBowlers;
       }
    }
    return top10EconomyBowlers;
}


