const csvtojson = require("csvtojson");
const deliveryPath = ("../data/deliveries.csv");
const matchesPath = ("../data/matches.csv");

const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        csvtojson()
            .fromFile(deliveryPath)
            .then((deliveries) => {
                const strikeratePerYear = matches.reduce((acc, cur) => {
                    let year = cur.season;
                    //console.log(year);
                    if (acc[year] === undefined) {
                        const matchId = matches.map((obj) => {
                            if (obj.season === year) {
                                return obj.id;
                            }
                        });
                        //console.log(matchId);
                        const delieveryId = deliveries.filter((items) => {
                            if (matchId.includes(items.match_id)) {
                                return items;
                            }
                        });
                        const batsmanStrikeRate = delieveryId.reduce((acc, curr) => {
                            let batsman = curr.batsman;
                            if (acc[batsman] === undefined) {
                                let batsmanRuns = delieveryId.reduce((acc, curr) => {
                                    if (curr.batsman === batsman) {
                                        acc += parseInt(curr.batsman_runs);
                                    }
                                    return acc;
                                }, 0)
                                let batsamnDelieveries = delieveryId.reduce((acc, curr) => {
                                    if (curr.batsman === batsman) {
                                        if (curr.wide_runs === "0" || curr.noball_runs === "0" || curr.bye_runs === "0" || curr.legbye_runs === "0") {
                                            acc += 1;
                                        }
                                    }
                                    return acc;
                                }, 0);
                                //console.log(batsamnDelieveries);
                                acc[batsman] = ((batsmanRuns / batsamnDelieveries) * 100).toFixed(2);
                                // console.log(acc[batsman]);
                            }
                            return acc;
                        }, {});
                        acc[year] = batsmanStrikeRate;
                    }
                    return acc;
                }, {});
                //console.log(strikeratePerYear);
                fs.writeFileSync("../public/output/strikeRateOfBatsmanForEachSeason.json", JSON.stringify(strikeratePerYear));

            });
    });

