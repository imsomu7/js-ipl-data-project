const csvtojson = require("csvtojson");
const matchesPath = '../data/matches.csv';
const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        //console.log(matches);
        let seasonMatches = {};
        for (let match of matches){
            if(seasonMatches.hasOwnProperty(match.season)){
                seasonMatches[match.season] ++;
            }
            else{
                seasonMatches[match.season] = 1;
            }
        }
        // let seasonMatches = {};
        // for (let index = 0; index < matches.length; index++) {

        //     if (seasonMatches[matches[index].season] != undefined) {
        //         seasonMatches[matches[index].season]++;
        //     }
        //     else {
        //         seasonMatches[matches[index].season] = 1;
        //     }
        // }
        fs.writeFileSync('../public/output/matchPerYear.json', JSON.stringify(seasonMatches));
    });

