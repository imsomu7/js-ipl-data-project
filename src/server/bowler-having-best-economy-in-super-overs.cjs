const csvtojson = require("csvtojson");
const deliveryPath = ("../data/deliveries.csv");
const matchesPath = ("../data/matches.csv");

const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        csvtojson()
            .fromFile(deliveryPath)
            .then((deliveries) => {
                // console.log(deliveries);
                const result = economicalSuperoverBowler(deliveries);
                //console.log(result);
                fs.writeFileSync('../public/output/bowlerHavingBestEconomyinSuperOvers.json', JSON.stringify(result));

            })
    })
function economicalSuperoverBowler(deliveries) {
    const superOverDelivers = deliveries.filter((obj) => {
        if (obj.is_super_over == 1) {
            return obj;
        }
    })
    //console.log(superOverDelivers);
    const totalRuns = superOverDelivers.reduce((acc, curr) => {
        if (acc[curr.bowler] === undefined) {
            acc[curr.bowler] = parseInt(curr.total_runs);
        }
        else {
            acc[curr.bowler] = acc[curr.bowler] + parseInt(curr.total_runs);
        }
        return acc;
    }, {});
    // console.log(totalRuns);
    const balls = superOverDelivers.reduce((acc, curr) => {
        if (curr.noball_runs == 0 || curr.wide_runs == 0) {
            if (acc[curr.bowler] === undefined) {
                acc[curr.bowler] = 1;
            }
            else {
                acc[curr.bowler] = acc[curr.bowler] + 1;
            }

        }
        return acc;
    }, {});
    // console.log(balls)
    let economy = {};
    for (let ball in balls) {
        if (economy[ball] === undefined) {
            economy[ball] = parseFloat(((totalRuns[ball] * 6) / balls[ball]).toFixed(2));
        }
    }
    //console.log(economy);
    let bowlerEconomy = Object.keys(economy).map(function (key) {
        return [key, economy[key]];
    }).sort(function (a, b) { return a[1] - b[1]; });
    // console.log(bowlerEconomy[0]);
    return bowlerEconomy[0];
}

