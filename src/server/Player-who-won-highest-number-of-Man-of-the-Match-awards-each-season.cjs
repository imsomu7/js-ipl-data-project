const csvtojson = require("csvtojson");
const matchesPath = '../data/matches.csv';
const fs = require('fs');
csvtojson()
    .fromFile(matchesPath)
    .then((matches) => {
        //console.log(matches);
        let perYear = {};
        for (let match of matches) {
            if (perYear[match.season] === undefined) {
                perYear[match.season] = manOfMatch(match.season);
            }
        }
        fs.writeFileSync("../public/output/PlayerwhogotmostMomEachYear.json", JSON.stringify(perYear));
        function manOfMatch(perYear) {
            let wonManofmatches = {};
            for (let match of matches) {
                if (wonManofmatches[match.player_of_match] === undefined && match.season === perYear) {
                    wonManofmatches[match.player_of_match] = 1
                }
                else {
                    if (perYear === match.season) {
                        wonManofmatches[match.player_of_match] += 1;
                    }
                }
            }
            wonManofmatches = Object.entries(wonManofmatches)
                .sort(([, award1], [, award2]) => {
                    if (award1 < award2) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                })
                .reduce((acc, curr) => {
                    if (acc[curr[0]] === undefined) {
                        acc[curr[0]] = curr[1];
                    }
                    return acc;
                }, {});
            wonManofmatches = Object.keys(wonManofmatches)[0];
            //console.log(wonManofmatches);
            return wonManofmatches;
        }
        //console.log(perYear);
        return perYear;
    }, {});